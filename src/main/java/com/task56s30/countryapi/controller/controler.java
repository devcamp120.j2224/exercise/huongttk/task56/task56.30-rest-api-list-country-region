package com.task56s30.countryapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56s30.countryapi.models.Country;
import com.task56s30.countryapi.models.Region;

@RestController
@CrossOrigin
public class controler {
    @GetMapping("/country-list")
    public ArrayList<Country> getCountry(){
        ArrayList<Region> regionListVN = new ArrayList<Region>();
        Region regionVN1 = new Region("HN", "Ha Noi");
        Region regionVN2 = new Region("DN", "Da Nang");
        Region regionVN3 = new Region("HCM", "Ho Chi Minh");
        Region regionVN4 = new Region("ND", "Nam Dinh");
        Region regionVN5 = new Region("HP", "Hai Phong");
        regionListVN.add(regionVN1);
        regionListVN.add(regionVN2);
        regionListVN.add(regionVN3);
        regionListVN.add(regionVN4);
        regionListVN.add(regionVN5);

        ArrayList<Region> regionListUSA = new ArrayList<Region>();
        Region regionUS1 = new Region("Washington", "Washington");
        Region regionUS2 = new Region("New York", "New York");
        
        regionListUSA.add(regionUS1);
        regionListUSA.add(regionUS2);
        

        ArrayList<Country> countryList = new ArrayList<Country>();
        Country countryVN = new Country("VN", "Viet Nam", regionListVN);
        Country countryUSA = new Country("USA", "America",regionListUSA);
      
            countryList.add(countryVN);
            countryList.add(countryUSA);

            return countryList;
            }
}
